# -*- coding:utf-8 -*-
"""Configuration schema declaration module.

The schema is based on the `voluptuous` library.
"""
import datetime
import re

from voluptuous import *

SNAME = "-\w{1,1}$"
LNAME = "--(\w|-)+"


def _validate_cli(cli):
    if cli is None:
        return cli
    if len(cli) == 0:
        raise ValueError("Option name cannot be empty")
    if len(cli) == 1 and not re.match(LNAME, cli[0]):
        raise ValueError("Invalid long option name: {0}".format(cli))
    if len(cli) == 2:
        if not re.match(SNAME, cli[0]):
            raise ValueError("Invalid short option name: {0}".format(cli))
        if not re.match(LNAME, cli[1]):
            raise ValueError("Invalid long option name: {0}".format(cli))
    if len(cli) > 2:
        raise ValueError("Option name cannot have more than 2 components")
    return cli


class Optional(Optional):
    """Optional configuration option.

    Attributes:
        cli (str): Command line option name.
        description (str): Option's description.
    """

    def __init__(self,
                 schema,
                 cli=None,
                 description=None,
                 msg=None,
                 default=UNDEFINED):
        super().__init__(schema, msg, default)
        self.cli = _validate_cli(cli)
        self.description = description


class Required(Required):
    """Required configuration option.

    Attributes:
        cli (str): Command line option name.
        description (str): Option's description.
    """

    def __init__(self,
                 schema,
                 cli=None,
                 description=None,
                 msg=None,
                 default=UNDEFINED):
        super().__init__(schema, msg, default)
        self.cli = _validate_cli(cli)
        self.description = description


class TimeSpan(object):
    """String to datetime.timedelta converter.
    """

    def __init__(self, format=None, msg=None):
        self.msg = msg
        self.format = format or "%H:%M:%S"

    def __call__(self, value):
        try:
            duration = datetime.datetime.strptime(value, self.format)
            return datetime.timedelta(
                hours=duration.hour,
                minutes=duration.minute,
                seconds=duration.second)
        except (TypeError, ValueError):
            errormsg = self.msg or "unable to parse timespan {0}".format(value)
            raise CoerceInvalid(errormsg)
