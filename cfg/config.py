# -*- coding:utf-8 -*-
"""Configuration initialization module.
"""
import functools
import os
import sys

import attrdict
import click

from cfg import schema

CONFIG_OPTION_KEY = "--config"
CONFIG_OPTION_HELP = "Configuration file path."


def _get_type(definition):
    if isinstance(definition, schema.In):
        return click.Choice(definition.container)
    if isinstance(definition, type):
        return definition
    if isinstance(definition, (schema.Or, schema.And)):
        types = []
        for validator in definition.validators:
            result = _get_type(validator)
            if result is not None:
                types.append(result)
        for t in types:
            if type(t) == click.Choice:
                return t
        return t
    return None


def _get_attrs(option, definition):
    attrs = {"type": _get_type(definition)}
    if option.description:
        attrs["help"] = option.description
    if option.default and callable(option.default):
        attrs["default"] = option.default()
    attrs["show_default"] = True
    return attrs


def _get_cli_lname(option):
    cli = option.cli
    lname = cli[0] if len(cli) == 1 else cli[1]
    return lname.strip("-").replace("-", "_")


def _export(cfg_schema, index, cache, parent, command):
    for option, definition in cfg_schema.items():
        if option.cli:
            lname = _get_cli_lname(option)
            kwname = lname.strip("-").replace("-", "_")
            for i in option.cli:
                cache[i] = kwname
            index[lname] = (parent, option.schema)
            attrs = _get_attrs(option, definition)
            command = click.option(*option.cli, **attrs)(command)
        if isinstance(definition, schema.Schema):
            index[option.schema] = (parent, )
            command = _export(definition.schema, index, cache, option.schema,
                              command)
    return command


def _get_path(index, lname):
    path = []
    parent = index.get(lname, -1)
    while parent != -1:
        path.append(parent)
        parent = index.get(parent[0], -1)
    return path


def _override_conf(conf, index, kwargs):
    for k in kwargs:
        parent = conf
        for item in reversed(_get_path(index, k)):
            if item[0] is not None:
                child = parent.get(item[0], None)
                if child is None:
                    child = {}
                    parent[item[0]] = child
                parent = child
            if len(item) == 2:
                parent[item[1]] = kwargs[k]
    return conf


def _get_env_overrides(appname, options):
    names = {"{0}_{1}".format(appname, i).upper(): i for i in options.values()}
    overrides = {v: os.getenv(k, None) for k, v in names.items()}
    return {k: v for k, v in overrides.items() if v is not None}


def _get_cli_overrides(options, values):
    return {options[k]: values[options[k]] for k in sys.argv if k in options}


def loader(method=None, default_locations=()):
    """Create configuration loader.

    Attrs:
        method (callable(stream) -> dict): Configuration from stream parser.
        default_locations (iterable): Locations where to find configuraion.

    Returns:
        callable(file=None): Loader which can load configuration either from a
            file or default locations.
    """

    def cfgloader(file):
        """Load configuration either from a file or default locations.

        Args:
            file (stream): Configuration file.

        Returns:
            dict: The configuration parsed. If no default location was found
                returns `None`.

        Raises:
            ValueError: If the file argument is specified but not exists.
        """
        if method is None:
            return None
        if file is not None:
            if isinstance(file, str):
                with open(file) as stream:
                    return method(stream)
            return method(file)
        for path in default_locations:
            if os.path.exists(path):
                with open(path) as stream:
                    return method(stream)
        return None

    return cfgloader


def export(cfg_schema, appname, loader=None):
    """Export configuration options to the command line interface.

    The order of configuration provessing:
        1. Read from configuration file.
        2. Override configuration from environment variables.
        3. Override configuration from command line.

    Args:
        cfg_schema (cfg.schema.Schema): Configuration schema.
        loader: (callable(file) -> dict): Configuration file loader.
    """

    def decorator(command):

        index = {}
        options = {}

        @functools.wraps(command)
        def wrapper(*args, **kwargs):
            conf_file = kwargs.get("config", None)
            conf = loader(conf_file) or {} if loader else {}

            env_overrides = _get_env_overrides(appname, options)
            conf = _override_conf(conf, index, env_overrides)

            cli_overrides = _get_cli_overrides(options, kwargs)
            conf = _override_conf(conf, index, cli_overrides)

            command(*args, configuration=attrdict.AttrDict(**conf), **kwargs)

        wrapper = click.option(
            CONFIG_OPTION_KEY,
            type=click.File(),
            default=None,
            help=CONFIG_OPTION_HELP)(wrapper)
        return _export(cfg_schema, index, options, None, wrapper)

    return decorator
