# -*- coding:utf-8 -*-
"""This module contains functions to read YAML configuration files.
"""
import os

import yaml

E_DICT_JOIN_TYPE_MISSMATCH = "Unable to join dicts: type missmatch at `{0}`"
E_LOAD_PARAM_NONE = "Parameter `file` cannot be None"


def dict_join(dict_a, dict_b):
    """Join dictionaries.

    The content of `dict_a` will be overriden by content of `dict_b` in the
    following manner:
        1. If dict_a[key] is a dict it will be joined reqursive.
        2. If dict_a[key] is a list it will be extended.
        3. If dict_a[key] is a set it will be extended.
        4. If dict_a[key] is other type it will be overriden.

    Arguments:
        dict_a (dict): Dictionary to be extended or overriden.
        dict_b (dict): Dictionary with a new values.

    Returns:
        dict: dict_a.
    """
    for k in dict_b:
        b = dict_b[k]
        if k in dict_a:
            a = dict_a[k]
            if type(a) != type(b):
                raise ValueError(E_DICT_JOIN_TYPE_MISSMATCH.format(k))
            if isinstance(a, dict):
                dict_join(a, b)
            elif isinstance(a, list):
                a += b
            elif isinstance(a, set):
                a |= b
            else:
                dict_a[k] = b
        else:
            dict_a[k] = b
    return dict_a


def _normalize_path(basedir, filename):
    if not filename.startswith("/"):
        return os.path.join(basedir, filename)
    return filename


def _load_from_file(file):
    with open(file) as stream:
        return _load_from_stream(stream)


def _include_conf(conf, basedir, include):
    include_path = _normalize_path(basedir, include)
    included = _load_from_file(include_path)
    return dict_join(conf, included)


def _load_from_stream(stream):
    conf = yaml.load(stream)
    include = conf.pop("include", None)
    if include is not None:
        basedir = os.path.dirname(stream.name)
        if isinstance(include, str):
            conf = _include_conf(conf, basedir, include)
        else:
            for i in include:
                conf = _include_conf(conf, basedir, i)
    return conf


def load(file):
    """Load configuration in YAML format.

    Configuration file can include predefined parameter `include` which can
    be either list or string. If is's present the original configuration will
    be overriden with the configuration stored in the file `include`.

    Args:
        file (str, stream): File path or file stream.

    Returns:
        dict: Loaded configuration.
    """
    if file is None:
        raise ValueError(E_LOAD_PARAM_NONE)
    if isinstance(file, str):
        return _load_from_file(file)
    return _load_from_stream(file)
