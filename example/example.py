# -*- coding:utf-8 -*-
import click

from cfg import config
from cfg import schema
from cfg import yamlcfg

LOG_LEVELS = {"NOTSET", "DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"}

SMTP_PROVIDERS = {"LOG", "SENDGRID"}

SENDGRID_API_URL = "https://api.sendgrid.com/v3/"

LOGGING_SCHEMA = schema.Schema({
    schema.Required(
        "dir", ("--log-dir", ),
        default=".",
        description="Log files directory."):
    schema.IsDir,
    schema.Required(
        "level", ("-l", "--log-level", ),
        default="INFO",
        description="Log level."):
    schema.In(LOG_LEVELS)
})

RPC_SCHEMA = schema.Schema({
    schema.Optional(
        "host",
        cli=("--rpc-host", ),
        default="localhost",
        description="RPC brocker host."):
    schema.Or(None, str),
    schema.Optional(
        "port", cli=("--rpc-port", ), description="RPC broker port."):
    schema.Or(None, schema.All(int, schema.Range(min=0, max=65535))),
    schema.Optional(
        "user", cli=("--rcp-user", ), description="RPC broker login user."):
    schema.Or(None, str),
    schema.Optional(
        "password",
        cli=("--rpc-password", ),
        description="RPC broker login password."):
    schema.Or(None, str)
})

SENDGRID_API_SCHEMA = schema.Schema({
    schema.Required(
        "url", default=SENDGRID_API_URL, description="SendGrid API URL."):
    schema.Url,
    schema.Required(
        "key", cli=("--sendgrid-api-key", ), description="SendGrid API key."):
    str,
    schema.Optional("nested"):
    schema.Schema({
        schema.Optional(
            "test_nested",
            cli=("--test-nested", ),
            default="default-nested",
            description="Value of test nested section."):
        str
    })
})

CONF_SCHEMA = schema.Schema({
    schema.Required(
        "templates_dir",
        cli=("-t", "--templates_dir"),
        default="templates",
        description="Email templates location."):
    schema.IsDir,
    schema.Required(
        "smtp_provider",
        cli=("-s", "--smtp-provider", ),
        default="LOG",
        description="SMTP provider type."):
    schema.All(schema.In(SMTP_PROVIDERS)),
    schema.Optional("rpc", default={}):
    RPC_SCHEMA,
    schema.Optional("logging"):
    LOGGING_SCHEMA,
    schema.Optional("sendgrid_api"):
    SENDGRID_API_SCHEMA
})

CONTEXT_SETTINGS = {"help_option_names": ["-h", "--help"]}

LOADER = config.loader(
    method=yamlcfg.load, default_locations=("example/example.yml", ))


@click.group(context_settings=CONTEXT_SETTINGS)
def ctl():
    """Service control for maild.
    """


@ctl.command()
@config.export(CONF_SCHEMA.schema, "maild", loader=LOADER)
@click.option("-i", "--instance", default="maild", help="Instance name.")
@click.pass_context
def start(ctx, instance, configuration, **kwargs):
    """Start maild service.
    """
    print(configuration)
    # pass


@ctl.command()
@click.option("-i", "--instance", default="maild", help="Instance name.")
def stop(name):
    """Stop maild service.
    """
    print("stop", name)


@ctl.command()
@click.option("-i", "--instance", default="maild", help="Instance name.")
def status(name):
    """Status maild service.
    """
    print("status", name)


if __name__ == '__main__':
    ctl()
