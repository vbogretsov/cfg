# -*- coding:utf-8 -*-
"""A setuptools based setup module for 'yamlconf'.
"""

import setuptools


setuptools.setup(
    name="cfg",
    version="0.1.0",
    description="YAML based configuration library.",
    url="https://gitlab.com/vbogretsov/cfg",
    author="vbogretsov",
    author_email="bogrecov@gmail.com",
    license="BSD",
    packages=["cfg"],
    setup_requires=[
        "pytest-runner"
    ],
    tests_require=[
        "pytest==3.0.7",
        "pytest_click",
        "pytest-cov",
        "pytest-mock",
        "pyfakefs"
    ],
    install_requires=[
        "attrdict",
        "click",
        "pyyaml",
        "voluptuous"
    ]
)
