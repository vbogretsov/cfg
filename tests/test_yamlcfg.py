# -*- coding:utf-8 -*-
import pytest

from cfg import yamlcfg

CONF = """
include: conf.d/secure.yml
templates_dir: /var/lib/maild/templates
logging:
    dir: /var/log/maild
    level: INFO
rpc:
    host: rpcbus
"""

CONF_CONFD_SECURE = """
include:
    - postgres.yml
    - /tarantool.yml
rpc:
    password: "123456"
sendgrid_api:
    key: SECURE_KEY
"""

CONF_CONFD_POSTGRES = """
postgres:
    user: pguser
    password: pgpassword
"""

CONF_CONFD_TARANTOOL = """
tarantool:
    user: tuser
    password: tpassword
"""

EXPECTED_CONF = {
    "templates_dir": "/var/lib/maild/templates",
    "logging": {
        "dir": "/var/log/maild",
        "level": "INFO"
    },
    "rpc": {
        "host": "rpcbus",
        "password": "123456"
    },
    "sendgrid_api": {
        "key": "SECURE_KEY"
    },
    "postgres": {
        "user": "pguser",
        "password": "pgpassword"
    },
    "tarantool": {
        "user": "tuser",
        "password": "tpassword"
    }
}


def test_dict_join_adds_value():
    dict_a = {}
    dict_b = {"k": "str"}
    dict_a = yamlcfg.dict_join(dict_a, dict_b)
    assert dict_a["k"] == "str"


def test_dict_join_overrides_value():
    dict_a = {"k": "str1"}
    dict_b = {"k": "str2"}
    dict_a = yamlcfg.dict_join(dict_a, dict_b)
    assert dict_a["k"] == "str2"


def test_dict_join_exteds_set():
    dict_a = {"k": {"1", "2", "3"}}
    dict_b = {"k": {"2", "3", "4"}}
    dict_a = yamlcfg.dict_join(dict_a, dict_b)
    assert dict_a["k"] == {"1", "2", "3", "4"}


def test_dict_join_exteds_list():
    dict_a = {"k": ["1", "2", "3"]}
    dict_b = {"k": ["2", "3", "4"]}
    dict_a = yamlcfg.dict_join(dict_a, dict_b)
    assert dict_a["k"] == ["1", "2", "3", "2", "3", "4"]


def test_dict_join_type_missmatch_raises_value_error():
    with pytest.raises(ValueError):
        dict_a = {"k": {"1", "2", "3"}}
        dict_b = {"k": ["2", "3", "4"]}
        dict_a = yamlcfg.dict_join(dict_a, dict_b)


def test_dict_join_nested_adds_value():
    dict_a = {"nested": {}}
    dict_b = {"nested": {"k": "str"}}
    dict_a = yamlcfg.dict_join(dict_a, dict_b)
    assert dict_a["nested"]["k"] == "str"


def test_dict_join_nested_overrides_value():
    dict_a = {"nested": {"k": "str1"}}
    dict_b = {"nested": {"k": "str2"}}
    dict_a = yamlcfg.dict_join(dict_a, dict_b)
    assert dict_a["nested"]["k"] == "str2"


def test_dict_join_nested_exteds_set():
    dict_a = {"nested": {"k": {"1", "2", "3"}}}
    dict_b = {"nested": {"k": {"2", "3", "4"}}}
    dict_a = yamlcfg.dict_join(dict_a, dict_b)
    assert dict_a["nested"]["k"] == {"1", "2", "3", "4"}


def test_dict_join_nested_exteds_list():
    dict_a = {"nested": {"k": ["1", "2", "3"]}}
    dict_b = {"nested": {"k": ["2", "3", "4"]}}
    dict_a = yamlcfg.dict_join(dict_a, dict_b)
    assert dict_a["nested"]["k"] == ["1", "2", "3", "2", "3", "4"]


def test_dict_join_nested_type_missmatch_raises_value_error():
    with pytest.raises(ValueError):
        dict_a = {"nested": {"k": {"1", "2", "3"}}}
        dict_b = {"nested": {"k": ["2", "3", "4"]}}
        dict_a = yamlcfg.dict_join(dict_a, dict_b)


@pytest.fixture
def fsmock(fs):
    fs.CreateFile("confdir/example.yml", contents=CONF)
    fs.CreateFile("confdir/conf.d/secure.yml", contents=CONF_CONFD_SECURE)
    fs.CreateFile("confdir/conf.d/postgres.yml", contents=CONF_CONFD_POSTGRES)
    fs.CreateFile("/tarantool.yml", contents=CONF_CONFD_TARANTOOL)


def test_load_from_file(fsmock):
    conf = yamlcfg.load("confdir/example.yml")
    assert conf == EXPECTED_CONF


def test_load_from_stream(fsmock):
    with open("confdir/example.yml") as stream:
        conf = yamlcfg.load(stream)
        assert conf == EXPECTED_CONF


def test_load_rases_value_error_if_file_none():
    with pytest.raises(ValueError):
        yamlcfg.load(None)
