# -*- coding:utf-8 -*-
import json

import click
import pytest

from cfg import config
from cfg import schema

"""
TODO:
    test_export_sets_option_help
    test_export_sets_option_default
    test_export_sets_option_choises
    test_export_sets_option_file
    test_export_sets_short_name
    test_export_long_name_is_required
"""

LOG_LEVELS = {"NOTSET", "DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"}

SMTP_PROVIDERS = {"LOG", "SENDGRID"}

SENDGRID_API_URL = "https://api.sendgrid.com/v3/"

LOGGING_SCHEMA = schema.Schema({
    schema.Required(
        "dir", ("--log-dir", ),
        default=".",
        description="Log files directory."):
    schema.IsDir,
    schema.Required(
        "level", ("-l", "--log-level", ),
        default="INFO",
        description="Log level."):
    schema.In(LOG_LEVELS)
})

RPC_SCHEMA = schema.Schema({
    schema.Optional(
        "host",
        cli=("--rpc-host", ),
        default="localhost",
        description="RPC brocker host."):
    schema.Or(None, str),
    schema.Optional(
        "port", cli=("--rpc-port", ), description="RPC broker port."):
    schema.Or(None, schema.All(int, schema.Range(min=0, max=65535))),
    schema.Optional(
        "user", cli=("--rcp-user", ), description="RPC broker login user."):
    schema.Or(None, str),
    schema.Optional(
        "password",
        cli=("--rpc-password", ),
        description="RPC broker login password."):
    schema.Or(None, str)
})

SENDGRID_API_SCHEMA = schema.Schema({
    schema.Required(
        "url", default=SENDGRID_API_URL, description="SendGrid API URL."):
    schema.Url,
    schema.Required(
        "key", cli=("--sendgrid-api-key", ), description="SendGrid API key."):
    str,
    schema.Optional("nested"):
    schema.Schema({
        schema.Optional(
            "test_nested",
            cli=("--test-nested", ),
            default="default-nested",
            description="Value of test nested section."):
        str
    })
})

CONF_SCHEMA = schema.Schema({
    schema.Required(
        "templates_dir",
        cli=("-t", "--templates-dir"),
        default="templates",
        description="Email templates location."):
    schema.IsDir,
    schema.Required(
        "smtp_provider",
        cli=("-s", "--smtp-provider", ),
        default="LOG",
        description="SMTP provider type."):
    schema.All(schema.In(SMTP_PROVIDERS)),
    schema.Optional("rpc", default={}):
    RPC_SCHEMA,
    schema.Optional("logging"):
    LOGGING_SCHEMA,
    schema.Optional("sendgrid_api"):
    SENDGRID_API_SCHEMA
})

DEFAULT_CONF = {
    "templates_dir": "/var/lib/maild/templates",
    "logging": {
        "dir": "/var/log/maild",
        "level": "INFO"
    },
    "rpc": {
        "host": "rpcbus"
    }
}

CUSTOM_CONF = {
    "templates_dir": "/var/lib/maild/templates",
    "logging": {
        "dir": "/var/log/maild",
        "level": "DEBUG"
    },
    "rpc": {
        "host": "rpcbus",
        "port": 5673
    },
    "sendgrid_api": {
        "key": "secure-key"
    }
}


@pytest.fixture
def app(cli_runner, loader):

    @click.group()
    def ctl():
        pass

    @ctl.command()
    @config.export(CONF_SCHEMA.schema, "maild", loader=loader)
    @click.option("-i", "--instance", default="maild", help="Instance name.")
    @click.pass_context
    def start(ctx, instance, configuration, **kwargs):
        print(json.dumps(configuration))

    def runapp(args):
        return cli_runner.invoke(start, args)

    return runapp


@pytest.fixture
def default_conf(fs):
    path = "etc/app.conf"
    fs.CreateFile("etc/app.conf", contents=json.dumps(DEFAULT_CONF))
    return {"path": path, "value": DEFAULT_CONF}


@pytest.fixture
def custom_conf(fs):
    path = "~/app.conf"
    fs.CreateFile(path, contents=json.dumps(CUSTOM_CONF))
    return {"path": path, "value": CUSTOM_CONF}


@pytest.fixture
def loader(fs):
    locations = ("/etc/app.conf", "etc/app.conf")
    return config.loader(method=json.load, default_locations=locations)


def test_loader_returns_none_if_method_is_none(fs):
    fs.CreateFile("/etc/app.conf", json.dumps(DEFAULT_CONF))
    loader = config.loader()
    assert loader("/etc/app.conf") is None


def test_loader_returns_from_file_if_file_is_not_none(
        default_conf, custom_conf, loader):
    conf = loader(custom_conf["path"])
    assert conf == custom_conf["value"]


def test_loader_returns_from_default_locations_if_file_is_none(
        default_conf, custom_conf, loader):
    conf = loader(None)
    assert conf == default_conf["value"]


def test_loader_returns_none_if_no_default_location_found(loader):
    conf = loader(None)
    assert conf is None


def test_export_uses_loader(custom_conf, app):
    result = app(["--config", "~/app.conf"])
    assert json.loads(result.output) == custom_conf["value"]


def test_export_overiides_from_command_line(mocker, default_conf, app):
    expected_result = default_conf["value"]
    expected_result["templates_dir"] = "~/templates"
    expected_result["logging"]["level"] = "DEBUG"
    expected_result["sendgrid_api"] = {"key": "123456"}
    args = [
        "--templates-dir",
        expected_result["templates_dir"],
        "--log-level",
        expected_result["logging"]["level"],
        "--sendgrid-api-key",
        expected_result["sendgrid_api"]["key"]
    ]
    mocker.patch("sys.argv", args)
    actual_result = app(args)
    assert json.loads(actual_result.output) == expected_result


def test_export_overrides_from_env_variables(mocker, default_conf, app):
    expected_result = default_conf["value"]
    expected_result["templates_dir"] = "~/templates"
    expected_result["logging"]["level"] = "DEBUG"
    expected_result["sendgrid_api"] = {"key": "123456"}
    mocker.patch("os.environ", {
        "MAILD_TEMPLATES_DIR": expected_result["templates_dir"],
        "MAILD_LOG_LEVEL": expected_result["logging"]["level"],
        "MAILD_SENDGRID_API_KEY": expected_result["sendgrid_api"]["key"]
    })
    actual_result = app([])
    assert json.loads(actual_result.output) == expected_result


def test_export_overrides_from_env_then_from_cli(mocker, default_conf, app):
    expected_result = default_conf["value"]
    expected_result["templates_dir"] = "~/templates"
    expected_result["logging"]["level"] = "DEBUG"
    expected_result["sendgrid_api"] = {"key": "123456"}
    args = [
        "--log-level",
        expected_result["logging"]["level"],
        "--sendgrid-api-key",
        expected_result["sendgrid_api"]["key"]
    ]
    mocker.patch("sys.argv", args)
    mocker.patch("os.environ", {
        "MAILD_TEMPLATES_DIR": expected_result["templates_dir"],
        "MAILD_LOG_LEVEL": "CRITICAL",
        "MAILD_SENDGRID_API_KEY": "654321"
    })
    actual_result = app(args)
    assert json.loads(actual_result.output) == expected_result
