# -*- coding:utf-8 -*-
import datetime

import pytest

from cfg import schema


@pytest.mark.parametrize("cls", (schema.Optional, schema.Required))
def test_schema_valid_cli_name(cls):
    cls("schema", ("-l", "--log-level"))


@pytest.mark.parametrize("cls", (schema.Optional, schema.Required))
def test_schema_raises_if_cli_name_is_empty(cls):
    with pytest.raises(ValueError):
        cls("schema", ())


@pytest.mark.parametrize("cls", (schema.Optional, schema.Required))
def test_schema_raises_if_invalid_long_name_in_simple_cli(cls):
    with pytest.raises(ValueError):
        cls("schema", ("-log-level",))


@pytest.mark.parametrize("cls", (schema.Optional, schema.Required))
def test_schema_raises_if_only_short_cli_name_is_present(cls):
    with pytest.raises(ValueError):
        cls("schema", ("-l",))


@pytest.mark.parametrize("cls", (schema.Optional, schema.Required))
def test_schema_raises_if_invalid_long_name_in_complex_cli(cls):
    with pytest.raises(ValueError):
        cls("schema", ("-l", "-log-level"))


@pytest.mark.parametrize("cls", (schema.Optional, schema.Required))
def test_schema_raises_if_invalid_short_name_in_complex_cli(cls):
    with pytest.raises(ValueError):
        cls("schema", ("-lv", "--log-level"))


@pytest.mark.parametrize("cls", (schema.Optional, schema.Required))
def test_schema_raises_if_cli_to_long(cls):
    with pytest.raises(ValueError):
        cls("schema", ("-l", "--log-level", "--excess"))


def test_timespan_parsed():
    test_schema = schema.Schema({schema.Required("ttl"): schema.TimeSpan()})
    actual_timedelta = test_schema({"ttl": "01:10:15"})["ttl"]
    expected_timedelta = datetime.timedelta(hours=1, minutes=10, seconds=15)
    assert isinstance(actual_timedelta, datetime.timedelta)
    assert actual_timedelta == expected_timedelta


def test_timespam_raises_multipleinvalid_if_not_parsed():
    test_schema = schema.Schema({schema.Required("ttl"): schema.TimeSpan()})
    with pytest.raises(schema.MultipleInvalid):
        test_schema({"ttl": "01:70:15"})
